/*----- PROTECTED REGION ID(TangoSnmp::ClassFactory.cpp) ENABLED START -----*/
static const char *RcsId = "$Id: ClassFactory.cpp 6348 2013-04-17 11:28:30Z taurel $";
//=============================================================================
//
// file :        ClassFactory.cpp
//
// description : C++ source for the class_factory method of the DServer
//               device class. This method is responsible for the creation of
//               all class singleton for a device server. It is called
//               at device server startup.
//
// project :     Tango<-->Snmp link
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author: taurel $
//
// $Revision: 6348 $
// $Date: 2013-04-17 13:28:30 +0200 (mer., 17 avr. 2013) $
//
// $HeadURL: https://svn.code.sf.net/p/tango-ds/code/DeviceClasses/Communication/snmp/trunk/ClassFactory.cpp $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include <tango.h>
#include <TangoSnmpClass.h>

//	Add class header files if needed


/**
 *	Create TangoSnmp Class singleton and store it in DServer object.
 */

void Tango::DServer::class_factory()
{
	//	Add method class init if needed
	add_class(TangoSnmp_ns::TangoSnmpClass::init("TangoSnmp"));
}
/*----- PROTECTED REGION END -----*/	//	TangoSnmp::ClassFactory.cpp
